using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class fret_selected_text : MonoBehaviour
{
    [SerializeField]
    Slider fret_Selector;

    [SerializeField]
    Text fret_text;
   

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Text>().text = "Selected Fret: "+(int)fret_Selector.value;
    }
}
