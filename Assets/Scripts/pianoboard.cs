using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pianoboard : MonoBehaviour
{

    //1. This is a list of the notes with octaves available in a guitar normally tune to E3 with 24 frets, which is actually the piano range
   // => This should be replaced with something mapping the actual piano board (maybe a dictionnary)
    string[] piano_range = {"E3", "F3", "F#3", "G3", "G#3", "A3", "A#3", "B3", "C4", "C#4", "D4", "D#4", "E4", "F4", "F#4", "G4", "G#4", "A4", "A#4", "B4", "C5", "C#5", "D5", "D#5", "E5"
            , "F5", "F#5", "G5", "G#5", "A5", "A#5", "B5", "C6", "C#6", "D6", "D#6", "E6", "F6", "F#6", "G6", "G#6", "A6", "A#6", "B6", "C7", "C#7", "D7", "D#7", "E7" };
    // This is the array of the actual piano keys
    [SerializeField]
    RawImage[] pianokeys;
    
    
    // 2. The toggle box arrays of the different strings => we need that to know what notes are being played.
    [SerializeField]
    Toggle[] E_high_selectable = new Toggle[Guitar_fretboard.fret_board_length];
    [SerializeField]
    Toggle[] B_selectable = new Toggle[Guitar_fretboard.fret_board_length];
    [SerializeField]
    Toggle[] G_selectable = new Toggle[Guitar_fretboard.fret_board_length];
    [SerializeField]
    Toggle[] D_selectable = new Toggle[Guitar_fretboard.fret_board_length];
    [SerializeField]
    Toggle[] A_selectable = new Toggle[Guitar_fretboard.fret_board_length];
    [SerializeField]
    Toggle[] E_low_selectable = new Toggle[Guitar_fretboard.fret_board_length];

    //3. Fret selection elements -> we need to know if a capo is used and the selected region of the fretboard
    [SerializeField]
    Toggle capo_switch;
    [SerializeField]
    Slider fret_selection;
    int fret_selected;
    bool capo_on;


    // Array capturing the checked toggle on each string 
    int[] frettotranspose; 

    // Array converting frettotranspose to actual note - This is the value to pass to the piano class for update (we could do a get/set eventually)  => Should no longer be needed
    string[] transposednote;

    //Offset array
    int[] iniitalOffset = { 24, 19, 15, 10, 5, 0 };

    // Reference for the piano update - See 1. for rework
    [SerializeField]
    Text piano_keys;
    // [SerializeField]
    // We should have an instance of the entire piano board here

    // Start is called before the first frame update
    void Start()
    {
        //// Initializing the transposition array
        frettotranspose = new int[Guitar_fretboard.guitar_strings];
        transposednote = new string[Guitar_fretboard.guitar_strings];

        for (int i = 0; i < Guitar_fretboard.guitar_strings; ++i)
        {
            transposednote[i] = "";
        }

       
    }

    // Update is called once per frame
    void Update()
    {

        // This might be a piano roll function and need to move to another class as well as the corresponding properties
        CheckSelection();

    }

    void CheckSelection()
    {

      // init the value

        piano_keys.text = "";
        fret_selected = (int)fret_selection.value;
        capo_on = capo_switch.isOn;

        foreach (RawImage pianokey in pianokeys) {
            pianokey.GetComponent<pianokey>().isActive = false;
        }

        // Checking what is selected for each string from E high to E low and feeding this in an array from E high to E low => that is the index
        for (int i = 0; i < Guitar_fretboard.fret_board_length; ++i)
        {
            if (E_high_selectable[i].isOn)
            {
                frettotranspose[0] = i;
            }
            if (B_selectable[i].isOn)
            {
                frettotranspose[1] = i;
            }
            if (G_selectable[i].isOn)
            {
                frettotranspose[2] = i;
            }
            if (D_selectable[i].isOn)
            {
                frettotranspose[3] = i;
            }
            if (A_selectable[i].isOn)
            {
                frettotranspose[4] = i;
            }
            if (E_low_selectable[i].isOn)
            {
                frettotranspose[5] = i;
            }
        }

        
        
        
        // Calculating the index for the text stuff by going through the determined index of each string (from high E to low E)

        for (int i = 0; i < Guitar_fretboard.guitar_strings; ++i)
        {
            // Need to build some logic here for the offset

            // Checking if the note is actually play by comparing index with the last possible index. If yes, we don't had text
            if (frettotranspose[i] == (Guitar_fretboard.fret_board_length - 1))
            { transposednote[i] = "";
                // No note is being played so no need to highlight a note
                continue;
            }

            if (frettotranspose[i] == 0) {
                // if capo is not on we are playing open string notes
                if (!capo_on) {
                    transposednote[i] = piano_range[iniitalOffset[i]];
                    // highlight the note at 0
                    pianokeys[iniitalOffset[i]].GetComponent<pianokey>().isActive = true;

                        continue;
                }
                // if capo is on we are playing on the fret selected -1
                if (capo_on)
                {
                    transposednote[i] = piano_range[iniitalOffset[i] + fret_selected - 1];
                    pianokeys[iniitalOffset[i] + fret_selected - 1].GetComponent<pianokey>().isActive = true;
                    continue;
                }

            }

            // handle other cases with the simple offset (Eg the case when the selected notes are not not played or on the capo or opened)
            transposednote[i] = piano_range[iniitalOffset[i] + fret_selected + frettotranspose[i]-1];
            pianokeys[iniitalOffset[i] + fret_selected + frettotranspose[i] - 1].GetComponent<pianokey>().isActive = true;
        }

        // Creating the text to be displayed from the above
        for (int i = Guitar_fretboard.guitar_strings - 1; i >= 0; --i) {
            piano_keys.text += transposednote[i]+ " ";
        }   
        

        
        }

}
