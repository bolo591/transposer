using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Guitar_fretboard : MonoBehaviour
{
    //This is an entire class that manages everything for now. 

    string[] piano_range = {"E3", "F3", "F#3", "G3", "G#3", "A3", "A#3", "B3", "C4", "C#4", "D4", "D#4", "E4", "F4", "F#4", "G4", "G#4", "A4", "A#4", "B4", "C5", "C#5", "D5", "D#5", "E5"
            , "F5", "F#5", "G5", "G#5", "A5", "A#5", "B5", "C6", "C#6", "D6", "D#6", "E6", "F6", "F#6", "G6", "G#6", "A6", "A#6", "B6", "C7", "C#7", "D7", "D#7", "E7" };


    // This is the notes in the 6 strings of the guitar - This is used to change the text of the different toggles on the fretboard
    string[] E_high_string = { "E5", "F5", "F#5", "G5", "G#5", "A5", "A#5", "B5", "C6", "C#6", "D6", "D#6", "E6", "F6", "F#6", "G6", "G#6", "A6", "A#6", "B6", "C7", "C#7", "D7", "D#7", "E7" };
    string[] B_string = { "B4", "C5", "C#5", "D5", "D#5", "E5", "F5", "F#5", "G5", "G#5", "A5", "A#5", "B5", "C6", "C#6", "D6", "D#6", "E6", "F6", "F#6", "G6", "G#6", "A6", "A#6", "B6"};
    string[] G_string = { "G4", "G#4", "A4", "A#4", "B4", "C5", "C#5", "D5", "D#5", "E5", "F5", "F#5", "G5", "G#5", "A5", "A#5", "B5", "C6", "C#6", "D6", "D#6", "E6", "F6", "F#6", "G6"};
    string[] D_string = { "D4", "D#4", "E4", "F4", "F#4", "G4", "G#4", "A4", "A#4", "B4", "C5", "C#5", "D5", "D#5", "E5", "F5", "F#5", "G5", "G#5", "A5", "A#5", "B5", "C6", "C#6", "D6"};
    string[] A_string = { "A3", "A#3", "B3", "C4", "C#4", "D4", "D#4", "E4", "F4", "F#4", "G4", "G#4", "A4", "A#4", "B4", "C5", "C#5", "D5", "D#5", "E5", "F5", "F#5", "G5", "G#5", "A5"};
    string[] E_low_string = { "E3", "F3", "F#3", "G3", "G#3", "A3", "A#3", "B3", "C4", "C#4", "D4", "D#4", "E4", "F4", "F#4", "G4", "G#4", "A4", "A#4", "B4", "C5", "C#5", "D5", "D#5", "E5" };
    // Let's possibly think about doing a 9 strings guitar 


    // This is on many frets are exposed on the fretboard (including the empty one)
    public static int fret_board_length = 7;

    // int for number of strings on the guitar
    public static int guitar_strings = 6;

    // UI element references 
    // 1. The text array of the selectable frets
    [SerializeField]
    Text[] fret_selectable = new Text[fret_board_length];

    // 2. The toggle box arrays of the different strings
    [SerializeField]
    Toggle[] E_high_selectable = new Toggle[fret_board_length];
    [SerializeField]
    Toggle[] B_selectable = new Toggle[fret_board_length];
    [SerializeField]
    Toggle[] G_selectable = new Toggle[fret_board_length];
    [SerializeField]
    Toggle[] D_selectable = new Toggle[fret_board_length];
    [SerializeField]
    Toggle[] A_selectable = new Toggle[fret_board_length];
    [SerializeField]
    Toggle[] E_low_selectable = new Toggle[fret_board_length];
       
    //3. Fret selection elements
    [SerializeField]
    Toggle capo_switch;
    [SerializeField]
    Slider fret_selection;
    int fret_selected;
    bool capo_on;

    //4. Reset button reference
    [SerializeField]
    Button resetButton;

   

    
    void Start()
    {
        // Setting the toggle text for the not selected toggles to empty 
        E_high_selectable[6].GetComponentInChildren<Text>().text = "";
        B_selectable[6].GetComponentInChildren<Text>().text = "";
        G_selectable[6].GetComponentInChildren<Text>().text = "";
        D_selectable[6].GetComponentInChildren<Text>().text = "";
        A_selectable[6].GetComponentInChildren<Text>().text = "";
        E_low_selectable[6].GetComponentInChildren<Text>().text = "";


        // We are setting the fretboard and the controls to default
        Reinit();

      

        // FALSE means no capo on
        capo_on = capo_switch.isOn;
        fret_selected = (int)fret_selection.value;

        

        //Set the fretboard based on the fret_selection
        FretRefresh();


  

    }

    // Update is called once per frame
    void Update()
    {
        // Check is the slider moved, and update the selected fret value and doing screen refresh
        if (fret_selected != (int)fret_selection.value)
        {
            fret_selected = (int)fret_selection.value;
            FretRefresh();
        }

        // Check is the capo value has changed, and update the value of capo_on and doing screen refresh
        // IDEALLY WE SHOULD HAVE THE ABILITY TO SEE THE CAPO TOGGLE ONLY WHEN SELECTED FRET IS ABOVE 0
        if (capo_on != capo_switch.isOn)
        {
            capo_on = capo_switch.isOn;
            FretRefresh();
        }

        
        
    }
   
    // This function update the text on the fretboard to match the fret position and the capo usage
    void FretRefresh()
    {

        // WE DON'T CHANGE EVER THE NOT PLAYED TEXT (IT SHOULD BE EMPTY) WHICH INDEXED AT 6

        // Logic is shitty here... Let's do check if capo_on is on and decide on what to show at index 0 then handle the array from index 1 to 5 using the selected fret
        int origin_value = 0;

        if (capo_on)
        {
            origin_value = fret_selected - 1;
        } else
        {
            origin_value = 0;
        }

        // We first populate text on the origin raw either opened string note or capoed
            fret_selectable[0].text = (origin_value).ToString();
            E_high_selectable[0].GetComponentInChildren<Text>().text = E_high_string[origin_value];
            B_selectable[0].GetComponentInChildren<Text>().text = B_string[origin_value];
            G_selectable[0].GetComponentInChildren<Text>().text = G_string[origin_value];
            D_selectable[0].GetComponentInChildren<Text>().text = D_string[origin_value];
            A_selectable[0].GetComponentInChildren<Text>().text = A_string[origin_value];
            E_low_selectable[0].GetComponentInChildren<Text>().text = E_low_string[origin_value];
        

          for (int i = 1; i < fret_board_length - 1; ++i)
            {

                // Changing the selectable fretboard text
                fret_selectable[i].text = (i + fret_selected -1).ToString();

                // Changing the note of each toggle text to the selected note
                E_high_selectable[i].GetComponentInChildren<Text>().text = E_high_string[i + fret_selected -1];
                B_selectable[i].GetComponentInChildren<Text>().text = B_string[i + fret_selected -1];
                G_selectable[i].GetComponentInChildren<Text>().text = G_string[i + fret_selected -1];
                D_selectable[i].GetComponentInChildren<Text>().text = D_string[i + fret_selected -1];
                A_selectable[i].GetComponentInChildren<Text>().text = A_string[i + fret_selected -1];
                E_low_selectable[i].GetComponentInChildren<Text>().text = E_low_string[i + fret_selected -1];

            }
        


    }
   
    // This method is called in the start method and also linked to the reset button. 
    public void Reinit()
    {


        // Setting the fretboard to no selection
        E_high_selectable[6].isOn = true;
        B_selectable[6].isOn = true;
        G_selectable[6].isOn = true;
        D_selectable[6].isOn = true;
        A_selectable[6].isOn = true;
        E_low_selectable[6].isOn = true;



        // Reset the control
        capo_switch.isOn = false;
      
        fret_selection.value = fret_selection.minValue;
        
    }

}
