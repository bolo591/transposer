using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class pianokey : MonoBehaviour
{
    // color Inactive is either white or black. When active (eg played), it should be green
    Color colorInactive;
    Color colorActive = Color.green;
    RawImage key;

    // Bool to see if active or not. The pianoboard will access and change the value somehow.
    public bool isActive = false;

    
    
    // Start is called before the first frame update
    void Start()
    {
        // We should either serialized the color inactive or catch the default color here based on the prefab
        key = gameObject.GetComponent<RawImage>();
        colorInactive = key.color;

    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            key.color = colorActive;

        }
        else
        {
            key.color = colorInactive;
        }

    }
}
